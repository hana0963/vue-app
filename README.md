# README

## 설치

### node.js

node.js(https://nodejs.org/en/) 사이트에서 다운로드(8.11.4 LTS)

### Git

Git(https://git-scm.com/) 사이트에서 다운로드

### 소스 다운 및 설치
 
- 소스 다운

명령프롬프트창에서 다운받을 위치로 이동 후 아래 명령어 실행

```bash
> git clone https://gitlab.com/metalsm7/vue-app.git
> cd vue-app
```

- 설치

```bash
> npm install
> npm install supervisor -g
```

- webpack 빌드 & 서버 실행

```bash
> npm run build
> npm run supervisor
``` 

### 확인
 
1. http://localhost:3000/hello/World
2. http://localhost:3000/hello/Jack

해당 주소와 관련된 파일은 아래와 같습니다.

1. /routes/index.js
2. /views/hello.ejs
3. /src/hello.js
4. /src/app.vue

* vue.js, /src/hello.js, /src/app.vue 파일들은 webpack을 통해 /public/dist/hello.js 로 만들어지게 됩니다.

## 추가적으로
1. node.js + express : https://expressjs.com/ko/
2. ejs (express template engine) : https://github.com/mde/ejs
3. git : https://git-scm.com/book/ko/v1/%EC%8B%9C%EC%9E%91%ED%95%98%EA%B8%B0

