import Vue from 'vue';
import App from './app.vue';

const app1 = new Vue({
  el: '#app',
  data: {
    message: 'hello.js에서 실행!'
  }
});

const app2 = new Vue({
  el: '#app',
  components: { 'app-comp': App }
});
